/**
 *  RxJS in action
 *  Chapter #
 *  @author Paul Daniels
 *  @author Luis Atencio
 */
"use strict";

var express = require('express');
var Rx = require('@reactivex/rxjs');
var router = express.Router();

var pendingRequests = {};

router.get('/accounts/:id', (req, res) => {
    var id = req.params['id'];
    console.log("Got id: " + id);
    res.sendStatus(200);
});

router.post('/v1/requestToken', (req, res) => {
    var email = req.body.email;
    var password = req.body.password;
    console.log("got " + email + password);
    res.send({token: email + password});

});

router.post('/v1/dologin', (req, res) => {
    var token = req.body.token;
    console.log('Storing: ' + token);
    pendingRequests[token] = res;
});

router.post('/v1/verifyLogin', (req, res) => {
    var vToken = req.body.verifyToken;
    console.log(`Verifying log in for ${vToken}`);
    pendingRequests[vToken].send({token: 'dfajeaajdkfl;aewf'});
    res.sendStatus(200);
    pendingRequests[vToken] = null;
});

module.exports = router;