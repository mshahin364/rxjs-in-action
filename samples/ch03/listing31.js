/**
 *  RxJS in action 
 *  Chapter 3
 *  @author Paul Daniels
 *  @author Luis Atencio
 */

const search$ = Rx.Observable.fromEvent(inputText, 'keyup')
    .map(event => event.target.value)
    .flatMap(query => Rx.Observable.from
    (findAccounts(query)).defaultIfEmpty([]));

search$.forEach(
    function next(result) {  //#A
        if(result.length === 0) {
            clearResults(results);
        }
        else {
            appendResults(results, result);
        }
    },
    function error(err) {  //#A
        console.log('Error occurred while searching for accounts!' + err);
    },
    function complete() { //#B
        console.log('All done!');
    }
);
