/**
 *  RxJS in action 
 *  Chapter 4
 *  @author Paul Daniels
 *  @author Luis Atencio
 */

const loadButton = document.querySelector('#load');

   // Helper functions
const size = arr => arr.length;
const startsWith = (str, account) => account.name.startsWith(str);                    
const startsWithSaving = startsWith.bind(null, 'saving');

const accounts$ = Rx.Observable.fromPromise(
   makeHttpCall('/accounts')); // #A

Rx.Observable.fromEvent(loadButton, 'click') //#B
   .mergeMap(() => accounts$)  //#C
   .map(results => results.filter(startsWithSaving)) //#D
   .map(size) //#E
   .subscribe(console.log); //-> 1
