/**
 *  RxJS in action 
 *  Chapter 4
 *  @author Paul Daniels
 *  @author Luis Atencio
 */

const symbol = 'FB'; 
const currency = 'USD'; 
const webservice = `http://finance.yahoo.com/webservice/v1/symbols/${symbol}/${currency}/quote`;

const stockTicker$ = Rx.Observable.fromPromise(
     makeHttpCall(webservice, {   
       format: 'json'
     }))
   .concatMap(result => Rx.Observable.from(result.list.resources)) 
   .map(resource => resource.resource.fields); 
