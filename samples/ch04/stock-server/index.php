<?php
/** 
 * Simple script that fetches stock symbols (FB) using Yahoo Finance APIs
 * Chapter 4
 * Luis Atencio
 * Paul Daniels
 * Usage: $>php -S localhost:8000 -t /rxjs-in-action/samples/ch04/stock-server
 */
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');

if (preg_match('/\.(?:png|jpg|jpeg|gif)$/', $_SERVER["REQUEST_URI"])) {
    return false;    // serve the requested resource as-is.
}
else {    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, 'http://finance.yahoo.com/webservice/v1/symbols/fb/usd/quote?format=json');
    $result = curl_exec($ch);
    curl_close($ch);

    $obj = json_decode($result);

    echo json_encode($obj);
}
