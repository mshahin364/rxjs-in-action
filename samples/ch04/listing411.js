/**
 *  RxJS in action 
 *  Chapter 4
 *  @author Paul Daniels
 *  @author Luis Atencio
 */

function debounce(fn, time) {
  var timeoutId;   
  return function() {   
    const args = [fn, time]
        .concat(copyToArray(arguments)); 
    clearTimeout(timeoutId);  
    timeoutId = window.setTimeout.apply(window, args); 
  }                                           
} 
