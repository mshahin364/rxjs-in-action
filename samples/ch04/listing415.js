/**
 *  RxJS in action 
 *  Chapter 4
 *  @author Paul Daniels
 *  @author Luis Atencio
 */

// HTML elements
const amountFields = document.querySelectorAll('.amount-field'); 
const transferButton = document.querySelector('#transfer');
const completeMsg = document.querySelector('#message');

// Helper functions
const empty = val => !val || val === '' || val.length === 0; 
const validNum = val => !Number.isNaN(parseFloat(val)); 
const negate = function (func) {                      
   return function() {
      return !func.apply(null, arguments);
   };
};

Rx.Observable.fromEvent(amountFields, 'keyup')
  .debounceTime(500) 
  .map(event => event.target.value)
  .filter(negate(empty)) 
  .bufferWhen(() => Rx.Observable.fromEvent(transferButton, 'click')) 
  .map(amounts => amounts.every(validNum)) 
  .subscribe(isValid => {  
     if(!isValid) { 
        completeMsg.innerHTML = 'Entered invalid amounts!'; 
     }  
     else {
       completeMsg.innerHTML = 'Amounts are valid!';
       performTransfer(...);
     }  
});
