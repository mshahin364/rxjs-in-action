/**
 *  RxJS in action 
 *  Chapter 4
 *  @author Paul Daniels
 *  @author Luis Atencio
 */

Rx.Observable.combineLatest(stockTicker$, twoSecond$) 
  .map(company =>[company[0].name, new Money(currency, company[0].price)])
  .forEach(data => {
    [company, price] = data;  
    document.querySelector('#company').innerHTML = company;  
    document.querySelector('#price').innerHTML = price;  
});
