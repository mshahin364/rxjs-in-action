/**
 *  RxJS in action 
 *  Chapter 4
 *  @author Paul Daniels
 *  @author Luis Atencio
 */

const source$ = Rx.Observable.create(observer => {
    const now = Date.now();
    setTimeout(() => {
      observer.next(now);   
	}, 1000); 
});

source$.delay(2000) 
       .map(timestamp => Date.now() - timestamp) 
       .map(result => Math.floor(result / 1000))       
       .subscribe(seconds => console.log(`${seconds} seconds`));
