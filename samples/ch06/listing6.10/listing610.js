/**
 *  RxJS in action
 *  Chapter 6
 *  @author Paul Daniels
 *  @author Luis Atencio
 */
(function(root, Rx){
    "use strict";

    Rx.Observable.fromEvent(root, 'load')
    .subscribe(() => {

        var db = new PouchDB('transactions');

        var addMoney = Rx.Observable.interval(1000)
            .mapTo({_id: 'add', value: 100});
        var spendMoney = Rx.Observable.interval(330)
            .mapTo({_id: 'spend', value: 200});
        var transferMoney = Rx.Observable.interval(2500)
            .mapTo({_id: 'transfer', value: 500});

        Rx.Observable.merge(addMoney, spendMoney, transferMoney)
            .buffer(Rx.Observable.interval(5000))
            .mergeMap(transactions => db.bulkDocs(transactions))
            .subscribe(writeResult => {
                console.log("Updated transaction record")
            });



    });

})(window, Rx.KitchenSink);