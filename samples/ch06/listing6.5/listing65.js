/**
 *  RxJS in action
 *  Chapter #6
 *  @author Paul Daniels
 *  @author Luis Atencio
 */
(function (root, Rx) {
    "use strict";

    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object

        return Rx.Observable.fromReader(Array.prototype.filter.call(files, (f) => f.type.match('image.*')))
            .readAsDataURL();
    }

    function showImageThumbnail(file) {
        var span = document.createElement('span');
        span.innerHTML = ['<img class="img-thumbnail thumbnail" src="', file.image,
            '" title="', escape(file.fileName), '"/>'].join('');
        document.getElementById('list').insertBefore(span, null);
    }

    Rx.Observable.fromEvent(root, 'load')
        .subscribe(() => {

            var imageFileStream = Rx.Observable.fromEvent(
                document.getElementById('files'), 'change'
            );

            var amountObservable = Rx.Observable.fromEvent(
                document.getElementById('amount'), 'change',
                x => x.target.value
            );

            var sender = Rx.Observable.fromEvent(
                document.getElementById('fromPerson'), 'change',
                x => x.target.value
            );

            var images = imageFileStream
                .mergeMap(handleFileSelect, (_, x) =>
                    ({image: x.image.target.result,
                        fileName: x.file.name}))
                .do(showImageThumbnail);

            var submit = Rx.Observable.fromEvent(document.getElementById('submit'), 'click');

            submit
                .withLatestFrom(
                    images, amountObservable, sender,
                    (_, file, amount, from) => ({file, amount, from})
                )
                .subscribe((x) => {
                    alert(`Check from: ${x.from}, of value $${x.amount} successfully deposited!`);
                });
        });

})(window, Rx.KitchenSink);
