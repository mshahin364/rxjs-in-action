/**
 *  RxJS in action
 *  Chapter #
 *  @author Paul Daniels
 *  @author Luis Atencio
 */
(function(Rx){
    "use strict";
    Rx.Observable.fromReader = (fileList) => {

        var files = new Array(fileList.length);

        for (var i = 0, f; f = fileList[i]; i++) {
            files[i] = f;
        }

        return {
            readAsDataURL: () => {

                return Rx.Observable.from(files)
                    .mergeMap(file => {
                        var reader = new FileReader();
                        var loaded = Rx.Observable.fromEvent(reader, 'load')
                            .map(image => ({file, image}));

                        var errored = Rx.Observable.fromEvent(reader, 'error');

                        return Rx.Observable.using(
                            () => reader.readAsDataURL(file),
                            _ => loaded.race(errored.mergeMap(e => Rx.Observable.throw(e)))
                        );
                    });

            }
        };

    };
})(Rx.KitchenSink);