/**
 *  RxJS in action
 *  Chapter #6
 *  @author Paul Daniels
 *  @author Luis Atencio
 */
"use strict";

(function(root, Rx) {

    var Observable = Rx.Observable;

    var hashToken = 'test';
    const baseUrl = '/api/samples/ch06/1';

    function saveSession(token) {
        document.cookie = `authToken=${token};expires=`;
    }

    function showBanner(msg) {
        window.open('http://localhost:3000/ch06/listing6.1/verify.html', 'Verify Email');
    }

    function redirect(path) {

    }

    var $continue = $('#continue');
    var $verify = $('#verify');
    var $emailAddress = $('#emailAddress');

    var continueLogin = Observable.fromEvent($continue, 'click');

    var emailSent = Observable.fromEvent($verify, 'click')
        .withLatestFrom(Rx.Observable.fromEvent($emailAddress, 'keyup', (e) => {
            return $emailAddress.val();
        }), (_, emailAddress) => ({emailAddress}))
        .exhaustMap((body) => {
            return $.post(`${baseUrl}/sendEmail`, {emailAddress: body.emailAddress, hashToken: hashToken})
        })
        .do(() => {
            showBanner('Email Sent!');
        })
        .flatMap(_ => $.getJSON(`${baseUrl}/emailVerified/${hashToken}`))
        .share();

    Observable.zip(
        emailSent,
        continueLogin.skipUntil(emailSent),
        (token, _) => token
        )
        //.take(1)
        .subscribe(token => {
            saveSession(token);
            redirect('/');
        }, (e) => console.log("Error occurred"));

})(window, Rx.KitchenSink);